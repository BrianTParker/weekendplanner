class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.string :description
      t.date :eventBeginDate
      t.date :eventEndDate
      t.string :createdBy

      t.timestamps null: false
    end
  end
end
