class CreateIngredients < ActiveRecord::Migration
  def change
    create_table :ingredients do |t|
      t.string :description
      t.string :amount
      t.references :user, index: true, foreign_key: true
      t.references :meal, index: true, foreign_key: true
      t.string :created_by

      t.timestamps null: false
    end
  end
end
