# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150714234708) do

  create_table "attendings", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "event_id",   limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "attendings", ["event_id"], name: "index_attendings_on_event_id", using: :btree
  add_index "attendings", ["user_id"], name: "index_attendings_on_user_id", using: :btree

  create_table "dishes", force: :cascade do |t|
    t.string   "description", limit: 255
    t.integer  "user_id",     limit: 4
    t.integer  "meal_id",     limit: 4
    t.integer  "event_id",    limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "dishes", ["event_id"], name: "index_dishes_on_event_id", using: :btree
  add_index "dishes", ["meal_id"], name: "index_dishes_on_meal_id", using: :btree
  add_index "dishes", ["user_id"], name: "index_dishes_on_user_id", using: :btree

  create_table "events", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.string   "description",    limit: 255
    t.date     "eventBeginDate"
    t.date     "eventEndDate"
    t.string   "createdBy",      limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "ingredients", force: :cascade do |t|
    t.string   "description", limit: 255
    t.string   "amount",      limit: 255
    t.integer  "user_id",     limit: 4
    t.integer  "meal_id",     limit: 4
    t.string   "created_by",  limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "dish_id",     limit: 4
  end

  add_index "ingredients", ["meal_id"], name: "index_ingredients_on_meal_id", using: :btree
  add_index "ingredients", ["user_id"], name: "index_ingredients_on_user_id", using: :btree

  create_table "meals", force: :cascade do |t|
    t.string   "description", limit: 255
    t.string   "comments",    limit: 255
    t.integer  "event_id",    limit: 4
    t.integer  "day",         limit: 4
    t.integer  "user_id",     limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "meals", ["event_id"], name: "index_meals_on_event_id", using: :btree
  add_index "meals", ["user_id"], name: "index_meals_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",         limit: 255
    t.string   "password_hash", limit: 255
    t.string   "password_salt", limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "userName",      limit: 255
  end

  add_foreign_key "attendings", "events"
  add_foreign_key "attendings", "users"
  add_foreign_key "dishes", "events"
  add_foreign_key "dishes", "meals"
  add_foreign_key "dishes", "users"
  add_foreign_key "ingredients", "meals"
  add_foreign_key "ingredients", "users"
  add_foreign_key "meals", "events"
  add_foreign_key "meals", "users"
end
