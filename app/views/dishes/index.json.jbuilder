json.array!(@dishes) do |dish|
  json.extract! dish, :id, :description, :user_id, :meal_id, :event_id
  json.url dish_url(dish, format: :json)
end
