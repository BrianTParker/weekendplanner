class Dish < ActiveRecord::Base
  belongs_to :user
  belongs_to :meal
  belongs_to :event
  has_many :ingredient
end
