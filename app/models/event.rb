class Event < ActiveRecord::Base
	has_many :users, through: :attendings
	has_many :attendings
	validates_presence_of :name
	
end
