class Meal < ActiveRecord::Base
  belongs_to :event
  belongs_to :user
  has_many :ingredient
  has_many :dish
  validates_presence_of :description
end
