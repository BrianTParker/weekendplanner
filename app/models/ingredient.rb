class Ingredient < ActiveRecord::Base
  belongs_to :user
  belongs_to :meal
  belongs_to :dish
  validates_presence_of :description
  validates_presence_of :amount
end
