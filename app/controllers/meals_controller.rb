class MealsController < ApplicationController
  before_action :set_meal, only: [:show, :edit, :update, :destroy]

  # GET /meals
  # GET /meals.json
  def index
    @meals = Meal.all
  end

  # GET /meals/1
  # GET /meals/1.json
  def show
    @mealId = params[:id]
    @meal = Meal.joins(:user).select('meals.event_id,meals.description as meal_description').where('meals.id' => params[:id])
    @mealName = Meal.find(params[:id])
    @eventInfo = Event.find(@mealName.event_id)
    @dish = Dish.joins(:user).select('users.userName, users.id as user_id, dishes.description, dishes.id').where('dishes.meal_id' => params[:id])
    @ingredients = Ingredient.joins(:user).select('users.id as user_id, users.userName, ingredients.id, ingredients.description, ingredients.amount, ingredients.dish_id').where('ingredients.meal_id' => params[:id])
  end

  # GET /meals/new
  def new
    @meal = Meal.new

  end

  # GET /meals/1/edit
  def edit
  end

  # POST /meals
  # POST /meals.json
  def create
    @meal = Meal.new(meal_params)

    respond_to do |format|
      if @meal.save
        format.html { redirect_to @meal, notice: 'Meal was successfully created.' }
        format.json { render :show, status: :created, location: @meal }
      else
        format.html { render :new }
        format.json { render json: @meal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /meals/1
  # PATCH/PUT /meals/1.json
  def update
    respond_to do |format|
      if @meal.update(meal_params)
        format.html { redirect_to @meal, notice: 'Meal was successfully updated.' }
        format.json { render :show, status: :ok, location: @meal }
      else
        format.html { render :edit }
        format.json { render json: @meal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /meals/1
  # DELETE /meals/1.json
  def destroy
    @meal.destroy
    respond_to do |format|
      format.html { redirect_to meals_url, notice: 'Meal was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_meal
      @meal = Meal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def meal_params
      params.require(:meal).permit(:description, :comments, :event_id, :day, :user_id, :created_at, :updated_at)
    end
end
