class WelcomeController < ApplicationController

	skip_before_filter :require_login
  def index

  	if(session[:user_id])
  		redirect_to("/events")
  	end
  	
  end

  
end
