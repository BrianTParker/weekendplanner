class SessionsController < ApplicationController
	skip_before_filter :require_login
	def create
		@user = User.authenticate(params[:email], params[:password])

		if(@user)
			#flash[:notice] = "You've been logged in."
			session[:user_id] = @user.id
			session[:userName] = @user.userName
			redirect_to "/"
		else
			flash[:alert] = "There was a problem logging you in"
			redirect_to "/log-in"
		end
	end

	def destroy

		session[:user_id] = nil
		flash[:notice] = "You've been logged out successfully."
		redirect_to "/"
		
	end
end
