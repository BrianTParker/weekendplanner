class UsersController < ApplicationController
	skip_before_filter :require_login
	require "uri"
	require "net/http"
	def new
		
		@user = User.new
	end

	def show
		@user = User.find(params[:id])
	end

	def create 
		@user = User.new(user_params)
		respond_to do |format|
			if @user.save
				session[:user_id] = @user.id
				session[:userName] = @user.userName
				format.html { redirect_to "/", notice: 'User was successfully created.' }


				
			else
				format.html { render :new }
	        	format.json { render json: @user.errors, status: :unprocessable_entity }
			end 
		end
	end

	private

	def user_params
		params.require(:user).permit(:userName, :email, :password, :password_confirmation)
	end

end
